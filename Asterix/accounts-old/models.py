from django.db import models

GENDER_CHOICE = (
	('male', 'MALE'),
	('female', 'FEMALE'),
	)
COLLEGE_CHOICE = (
	('cos', 'COS'),
	('cot', 'COT'),
	)
YEAR_CHOICE = (
	('2019', '2019'),
	('2018', '2018'),
	('2017', '2017'),
	('2016', '2016'),
	('2015', '2015'),
	('2014', '2014'),
	)
DEPARTMENT_CHOICE = (
	('chm', 'Chemistry'),
	('csc', 'Computer Science'),
	('emt', 'EMT'),
	('geo', 'Geology'),
	('gpy', 'Geophysics'),
	('indchm', 'Inustrial Chemistry'),
	('mth', 'Mathematics'),
	('phy', 'Physics'),
	('chmeng', 'Chemical Engineering'),
	('electeng', 'Electrical Engineering'),
	('mareng', 'Marine Engineering'),
	('mecheng', 'Mechanical Engineering'),
	('peteng', 'Petroleum Engineering'),
	)


# Create your models here.

class UserProfile(models.Model):
    First_name = models.CharField(max_length=100)
    Last_name = models.CharField(max_length=100)
    Username = models.CharField(max_length=100)
    Email = models.EmailField(max_length=150)
    Tell = models.CharField(max_length=12)
    Gender = models.CharField(max_length=6, choices=GENDER_CHOICE, default='green')
    Mat_idx = models.CharField(max_length=3, choices=COLLEGE_CHOICE, default='cos')
    Mat_num = models.CharField(max_length=4)
    Mat_yr = models.CharField(max_length=4, choices=YEAR_CHOICE, default='2019')
    Dept = models.CharField(max_length=8, choices=DEPARTMENT_CHOICE, default='chm')
    Pwd = models.CharField(max_length=100)
    Date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.Username


