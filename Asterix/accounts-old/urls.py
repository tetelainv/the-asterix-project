from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

app_name = 'accounts'

urlpatterns = [
    path('', HomeViews, name='home'),
    path('register/', RegisterViews, name='register'),
]