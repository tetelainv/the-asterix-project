from django.shortcuts import render, redirect, HttpResponse, get_object_or_404, HttpResponsePermanentRedirect
from django.contrib.auth.forms import UserCreationForm
from .models import *
# Create your views here.


def HomeViews(request):
	count = UserProfile.objects.all().count()
	context = {
		'count': count
	}
	return render(request, 'accounts/index.html', context)


def RegisterViews(request):
	if request.method == 'POST':
		f_name = request.POST.get("first_name")
		l_name = request.POST.get("last_name")
		username = request.POST.get("username")
		email = request.POST.get("email")
		tell = request.POST.get("tell")
		gender = request.POST.get("gender")
		mat_idx = request.POST.get("mat_ind")
		mat_num = request.POST.get("mat_num")
		mat_yr = request.POST.get("mat_yr")
		dept = request.POST.get("dept")
		pwd = request.POST.get("pwd")
		userprofile = UserProfile()
		userprofile.First_name = f_name
		userprofile.Last_name = l_name
		userprofile.Username = username
		userprofile.Email = email
		userprofile.Tell = tell
		userprofile.Gender = gender
		userprofile.Mat_idx = mat_idx
		userprofile.Mat_min = mat_num
		userprofile.Mat_yr  = mat_yr
		userprofile.Dept  = dept
		userprofile.Pwd = pwd
		userprofile.save()
		print(f_name, l_name, username, email, tell, gender, mat_idx, mat_num, mat_yr, dept, pwd)
		return redirect("accounts:home")
	return render(request, "accounts/login.html", {})

#		form = UserCreationForm(request.POST)
#		if form.is_valid():
#			user = form.save(commit=False)
#			user.save()
#			return HttpResponse('User created susccesfully')
#	else:
#		form = UserCreationForm
#	return render(request, "accounts/login.html", {'form': form})
