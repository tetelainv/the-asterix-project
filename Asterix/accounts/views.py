from django.contrib.auth import login, get_user_model, logout
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

# Create your views here.

User = get_user_model()

from .forms import UserCreationForm, UserLoginForm
from .models import ActivationProfile

@login_required(login_url='/user/login/')
def auth(request):
    return HttpResponseRedirect("/login/")

    """if request.user.authenticate():
        print(request.user.profile.city)
    return render(request, "accounts/auth.html", {})"""


@login_required(login_url='/user/login/')
def home(request):
    return render(request, "accounts/home.html", {})



def register(request, *args, **kwargs):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/user/login/")
    return render(request, "accounts/register.html", {"form": form})


def login_view(request, *args, **kwargs):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        if 'next' in request.POST:
            return redirect(request.POST['next'])
        return HttpResponseRedirect("/user/login/")
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/user/login/")


def activate_user_view(request, code=None, *args, **kwargs):
    if code:
        act_profile_qs = ActivationProfile.objects.filter(key=code)
        if act_profile_qs.exists() and act_profile_qs.count() == 1:
            act_obj = act_profile_qs.first()
            if not act_obj.expired:
                user_obj = act_obj.user
                user_obj.is_active = True
                user_obj.save()
                act_obj.expired = True
                act_obj.save()
                return HttpResponseRedirect("/user/login/")
    # invalid code
    return HttpResponseRedirect("/user/login/")
