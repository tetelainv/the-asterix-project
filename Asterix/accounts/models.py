from django.conf import settings
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

# Create your models here.
from .utils import code_generator

USERNAME_REGEX = '^[a-zA-Z0-9.+-]*$'
NAME_REGEX = '^[a-zA-Z]*$'
TELEPHONE_REGEX = '[^0-9]*$'

GENDER_CHOICE = (
    ('', 'Gender'),
    ('male', 'MALE'),
    ('female', 'FEMALE'),
    )
COLLEGE_CHOICE = (
	('', 'Select'),
	('cos', 'COS'),
	('cot', 'COT'),
	)
YEAR_CHOICE = (
    ('', 'Select'),
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('2014', '2014'),
    )
DEPARTMENT_CHOICE = (
    ('', 'Select'),
    ('chm', 'Chemistry'),
    ('csc', 'Computer Science'),
    ('emt', 'EMT'),
    ('geo', 'Geology'),
    ('gpy', 'Geophysics'),
    ('indchm', 'Inustrial Chemistry'),
    ('mth', 'Mathematics'),
    ('phy', 'Physics'),
    ('chmeng', 'Chemical Engineering'),
    ('electeng', 'Electrical Engineering'),
    ('mareng', 'Marine Engineering'),
    ('mecheng', 'Mechanical Engineering'),
    ('peteng', 'Petroleum Engineering'),
    )
LEVEL_CHOICE = (
    ('', 'Select'),
    ('100', '100'),
    ('200', '200'),
    ('300', '300'),
    ('400', '400'),
    ('500', '500'),
)
class MyUserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            username = username,
            email=self.normalize_email(email),
            first_name = first_name,
            last_name = last_name,
            college = college,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_admin(self, username, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            username = username,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_admin(
            username,
            email,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user



def validate_matricNumber(value):
        if value <= 1000:
            if value>= 5999:
                raise ValidationError(
                    _('%(value)s cant be your Matric Number'),
                    params={'value': value},
                    )


class MyUser(AbstractBaseUser):
    username = models.CharField(
                max_length=50,
                validators=[
                    RegexValidator(
                        regex = USERNAME_REGEX,
                        message = 'Username must be Alpahnumeric or contain any of the following: ". @ + -" ',
                        code='invalid_username'
                    )],
                unique=True,
            )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(
                max_length=20,
                validators=[
                    RegexValidator(
                        regex = NAME_REGEX,
                        message = 'First name must be letters only',
                        code='invalid_first_name'
                    )],
            )
    last_name = models.CharField(
                max_length=20,
                validators=[
                    RegexValidator(
                        regex = NAME_REGEX,
                        message = 'Last name must be letters only',
                        code='invalid_last_name'
                    )],
            )
    telephone = models.CharField(
                max_length=12,
                validators=[
                    RegexValidator(
                        regex = TELEPHONE_REGEX,
                        message = 'Invalid Phonee number ',
                        code='invalid_telephone'
                    )],
            )
    college = models.CharField(
                    max_length = 3,
                    choices = COLLEGE_CHOICE,
                    default = ''
            )
    gender = models.CharField(
                    max_length =6,
                    choices = GENDER_CHOICE,
                    default = ''
            )
    matric_year = models.CharField(
                    max_length = 4,
                    choices = YEAR_CHOICE,
                    default = ''
            )
    department = models.CharField(
                    max_length = 8,
                    choices = DEPARTMENT_CHOICE,
                    default = ''
            )
    level = models.CharField(
                    max_length = 3,
                    choices = LEVEL_CHOICE,
                    default = ''
            )
    matric_number   = models.IntegerField(
                    validators=[validate_matricNumber]
                    ) 
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    # @property
    # def is_staff(self):
    #     "Is the user a member of staff?"
    #     # Simplest possible answer: All admins are staff
    #     return self.is_admin



class ActivationProfile(models.Model):
    user    = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    key     = models.CharField(max_length=120)
    expired = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.key = code_generator()
        super(ActivationProfile, self).save(*args, **kwargs)


def post_save_activation_receiver(sender, instance, created, *args, **kwargs):
    if created:
        #send email
        print('activation created')

post_save.connect(post_save_activation_receiver, sender=ActivationProfile)




class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    city = models.CharField(max_length=120, null=True, blank=True)

    def __str__(self):
        return str(self.user.username)

    def __unicode__(self):
        return str(self.user.username)


def post_save_user_model_receiver(sender, instance, created, *args, **kwargs):
    if created:
        try:
            Profile.objects.create(user=instance)
            ActivationProfile.objects.create(user=instance)
        except:
            pass

post_save.connect(post_save_user_model_receiver, sender=settings.AUTH_USER_MODEL)
