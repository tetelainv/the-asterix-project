from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

app_name = 'accounts'

urlpatterns = [
    path('', home),
    path('user/register/', register),
    path('auth/', auth),
    path('user/login/', login_view, name='login'),
    path('user/logout/', logout_view, name='logout'),
    path('^activate/(?P<code>[a-z0-9].*)/$', activate_user_view),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)