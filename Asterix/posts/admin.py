from django.contrib import admin
from .models import *
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields  = {'Slug' : ('Title',)}

admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
