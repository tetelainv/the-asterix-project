from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

app_name = 'posts'

urlpatterns = [
    path('', PostViews, name='post'),
    path('detail/<slug:slug>/', PostDetail, name='detail'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)