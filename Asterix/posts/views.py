from django.shortcuts import render, redirect, HttpResponse, get_object_or_404, HttpResponsePermanentRedirect
from .models import *
from .form import *

# Create your views here.


def PostViews(request):
    posts = Post.objects.all()
    count = Post.objects.all().count()
    context = {
        'post': posts,
        'count': count,
        }
    return render(request, 'posts/posts.html', context)

def PostDetail(request, slug):
    count = Comment.objects.all().count()
    post = get_object_or_404(Post, Slug = slug)
    comments = post.comments.filter(Active=True, Parent__isnull=True)
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            Parent_obj = None
            try:
                Parent_id = int(request.POST.get('Parent_id'))
            except:
                Parent_id = None
            if Parent_id:
                Parent_obj = Comment.objects.get(id=Parent_id)
                if Parent_obj:
                    reply_comment = comment_form.save(commit=False)
                    reply_comment.Parent = Parent_obj
            new_comment = comment_form.save(commit=False)
            new_comment.Post = post
            new_comment.save()
            return redirect('posts:detail', slug)
    else:
        comment_form = CommentForm()
        context = {
            'post': post,
            'comment': comments,
            'comment_form': comment_form,
            'count': count
        }
    return render(request, 'posts/detail.html', context)





